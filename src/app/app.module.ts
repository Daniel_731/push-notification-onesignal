import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

// Push notification onesignal
import {OneSignal} from '@ionic-native/onesignal/ngx';
import {PushNotificationOnesignalService} from "./services/push-notification-onesignal.service";

@NgModule({
   declarations: [AppComponent],
   entryComponents: [],
   imports: [
      BrowserModule,
      IonicModule.forRoot(),
      AppRoutingModule
   ],
   providers: [
      StatusBar,
      SplashScreen,
      OneSignal,
      PushNotificationOnesignalService,
      {
         provide: RouteReuseStrategy,
         useClass: IonicRouteStrategy
      }
   ],
   bootstrap: [AppComponent]
})
export class AppModule {
}
