import {Injectable} from '@angular/core';
import {OneSignal} from '@ionic-native/onesignal/ngx';
import {Platform} from '@ionic/angular';

@Injectable({
   providedIn: 'root'
})
export class PushNotificationOnesignalService {

   constructor(private oneSignal: OneSignal, private platform: Platform) {}


   initPushNotificationsOnesignal() {
      try {

         if (this.platform.is('cordova')) {

            this.oneSignal.startInit('d1f8c759-c4b4-47d2-939f-827a8e429f77', '792494639525');

            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

            this.oneSignal.handleNotificationReceived().subscribe(() => {
               // do something when notification is received
               console.log('Received notification');
            });

            this.oneSignal.handleNotificationOpened().subscribe(() => {
               // do something when a notification is opened
               console.log('Opened notification');
            });

            this.oneSignal.endInit();

         } else {
            console.log('The Onesignal notifications not function in desktop browsers');
         }

      } catch (e) {
         console.error(e);
      }
   }

}
