import { TestBed } from '@angular/core/testing';

import { PushNotificationOnesignalService } from './push-notification-onesignal.service';

describe('PushNotificationOnesignalService', () => {
  let service: PushNotificationOnesignalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PushNotificationOnesignalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
